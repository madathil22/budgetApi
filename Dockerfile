FROM gradle:5.4-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle -Dorg.gradle.project.version=3 assembleService --no-daemon 

FROM adoptopenjdk/openjdk11:ubi
COPY --from=build /home/gradle/src/build/libs/*.jar app.jar
COPY --from=build /home/gradle/src/build/config/*.yml config/
ENTRYPOINT ["java","-jar","/app.jar"]